package com.app;

import com.app.gui.NickEntryWindow;
import com.app.server.ClientConnector;
import com.app.server.ClientEncoder;
import com.app.server.EventHandler;

import java.io.IOException;

public class ClientApplication {

    private static NickEntryWindow nickEntryWindow;
    private static ClientConnector connector;

    public static void main(String args[]) {
        nickEntryWindow = new NickEntryWindow();
        nickEntryWindow.setVisible(true);
        EventHandler.setNickEntryWindow(nickEntryWindow);
    }

    public static void sendMessageToServer(String message) {
        if (connector != null) {
            connector.sendMessage(message);
        }
    }

    public static void setClientConnector (ClientConnector conn) {
        connector = conn;
    }

    protected void finalize() throws IOException {
        sendMessageToServer(ClientEncoder.createPlayerLeftGameMessage());
    }

}
