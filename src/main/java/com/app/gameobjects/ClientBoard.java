package com.app.gameobjects;

import java.awt.*;

public class ClientBoard {

    private Tile[][] board;
    private boolean[] players;
    private int board_size_x, board_size_y;
    private int triangle_size;
    private Color[] colors;

    public ClientBoard(int triangle_size) {
        this.triangle_size = triangle_size;
        colors = new Color[6];
        players = new boolean[6];
        for (int i=0; i<6; i++)
            players[i] = true;
        colors[0] = new Color(255, 0, 0);
        colors[1] = new Color(0,0, 255);
        colors[2] = new Color(0, 255, 0);
        colors[3] = new Color(0, 255, 255);
        colors[4] = new Color(255, 255, 0);
        colors[5] = new Color(255, 0, 255);
        generateBoard();
    }

    public void generateBoard() {
        board_size_x = triangle_size * 4 + 5;
        board_size_y = triangle_size * 4 + 5;
        board = new Tile[board_size_y][board_size_x];
        createHexagramBoard(triangle_size);
    }

    private void createHexagramBoard(int triangle_size) {
        int marginX = 2;
        int marginY = 2;
        createUpperBigTriangle(triangle_size, marginX, marginY);
        createLowerBigTriangle(triangle_size, marginX, marginY);

        createLowerSmallTriangle(triangle_size, marginX, marginY);
        createUpperSmallTriangle(triangle_size, marginX, marginY);
        createUpperLeftSmallTriangle(triangle_size, marginX, marginY);
        createUpperRightSmallTriangle(triangle_size, marginX, marginY);
        createLowerLeftSmallTriangle(triangle_size, marginX, marginY);
        createLowerRightSmallTriangle(triangle_size, marginX, marginY);

    }

    private void createUpperBigTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 3 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if (j < board_size - marginY - triangle_size &&
                        i < board_size - marginX - triangle_size &&
                        i + j >= triangle_constant) {
                    board[j][i] = new Tile(i, j, Color.LIGHT_GRAY);
                }
            }
        }
    }

    private void createLowerBigTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = triangle_size * 5 + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if ( j >= marginY + triangle_size &&
                        i >= marginX + triangle_size && i+j <= triangle_constant) {
                    board[j][i] = new Tile(i, j, Color.LIGHT_GRAY);
                }
            }
        }
    }


    private void createUpperSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 3 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if (j < triangle_size + marginY &&
                        i < board_size - marginX - triangle_size && i + j >= triangle_constant) {
                    Tile tile = new Tile(i, j, colors[3]);
                    board[j][i] = tile;
                    if(players[0])
                        tile.setPawn(new Pawn(colors[0]));
                }
            }
        }
    }

    private void createUpperLeftSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 3 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if (j >= marginY + triangle_size &&
                        i >= triangle_size + marginX && i + j < triangle_constant) {
                    Tile tile = new Tile(i, j, colors[2]);
                    board[j][i] = tile;
                    if(players[1])
                        tile.setPawn(new Pawn(colors[5]));
                }
            }
        }
    }

    private void createUpperRightSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 5 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if (j >= marginY + triangle_size &&
                        i >= board_size - marginX - triangle_size && i + j <= triangle_constant) {
                    Tile tile = new Tile(i, j, colors[4]);
                    board[j][i] = tile;
                    if(players[2])
                        tile.setPawn(new Pawn(colors[1]));
                }
            }
        }
    }


    private void createLowerSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = triangle_size * 5 + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if ( j >= board_size - marginY - triangle_size &&
                        i >= marginX + triangle_size && i+j <= triangle_constant) {
                    Tile tile = new Tile(i, j, colors[0]);
                    board[j][i] = tile;
                    if(players[5])
                        tile.setPawn(new Pawn(colors[3]));
                }
            }
        }
    }

    private void createLowerLeftSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constant = 3 * triangle_size + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if ( j < board_size - marginY - triangle_size &&
                        i < marginX + triangle_size && i+j >= triangle_constant) {
                    Tile tile = new Tile(i, j, colors[1]);
                    board[j][i] = tile;
                    if(players[3])
                        tile.setPawn(new Pawn(colors[4]));
                }
            }
        }
    }

    private void createLowerRightSmallTriangle(int triangle_size, int marginX, int marginY) {
        int board_size = triangle_size * 4 + 5;
        int triangle_constatnt = triangle_size * 5 + 4;
        for (int i = 0; i< board_size_x; i++) {
            for (int j = 0; j < board_size_y; j++) {
                if ( j < board_size - marginY - triangle_size &&
                        i < board_size - marginX - triangle_size && i+j > triangle_constatnt) {
                    Tile tile = new Tile(i, j, colors[5]);
                    board[j][i] = tile;
                    if(players[4])
                        tile.setPawn(new Pawn(colors[2]));
                }
            }
        }
    }

    public Tile[][] getBoard() {
        return board;
    }

    public Tile[][] getTileList() {
        return board;
    }

    public int getBoardSizeX() {
        return board_size_x;
    }

    public int getBoardSizeY() {
        return board_size_y;
    }

    public void setPlayersArray(boolean[] array) {
        this.players = array;
    }
}
