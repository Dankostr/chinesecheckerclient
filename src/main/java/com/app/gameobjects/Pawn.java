package com.app.gameobjects;

import java.awt.*;

public class Pawn {

    private Color color;

    Pawn(Color color) {
        this.color = color;
    }

    public Color getColor() {
        return color;
    }
}
