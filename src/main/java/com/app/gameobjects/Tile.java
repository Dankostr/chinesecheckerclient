package com.app.gameobjects;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Tile {

    private Color color;
    private Pawn pawn;
    private int x;
    private int y;
    private Ellipse2D circle;

    public Tile (int x, int y, Color color) {
        this.color = color;
        this.x = x;
        this.y = y;
    }

    public Color getColor() {
        return color;
    }

    public void setPawn(Pawn pawn) {
        this.pawn = pawn;
    }

    public Pawn getPawn() {
        return pawn;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setCircle(Ellipse2D circle) {
        this.circle = circle;
    }

    public boolean containsPawn() {
        return (pawn != null);
    }

    public Ellipse2D getCircle() {
        return circle;
    }
}
