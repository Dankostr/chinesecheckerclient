package com.app.gui;

import com.app.ClientApplication;
import com.app.server.ClientEncoder;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class BotButtonActionListener implements ActionListener {

    private int slot;
    private JButton button;

    public BotButtonActionListener(int slot, JButton button) {
        this.slot = slot - 1;
        this.button = button;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(button.getText().equals("Dodaj bota")) {
            String message = ClientEncoder.createAddBotMessage(slot);
            ClientApplication.sendMessageToServer(message);
        }
        else if (button.getText().equals("Usun bota")) {
            String message = ClientEncoder.createRemoveBotMessage(slot);
            ClientApplication.sendMessageToServer(message);
        }
        else if (button.getText().equals("Obserwuj gre")) {
            String message = ClientEncoder.createLeaveSlotMessage(slot);
            ClientApplication.sendMessageToServer(message);
        }
    }
}
