package com.app.gui;

import com.app.ClientApplication;
import com.app.gameobjects.ClientBoard;
import com.app.gameobjects.Pawn;
import com.app.gameobjects.Tile;
import com.app.server.ClientCommunicator;
import com.app.server.ClientConnector;
import com.app.server.ClientEncoder;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

public class ClientPanel extends JPanel {

    private ClientBoard board;
    private int radius;
    private int spacing;
    private int marginX;
    private int marginY;
    private ArrayList<Tile> tiles;
    private Tile active_tile;
    private ArrayList<Point> available_tiles;
    private Color available_color = new Color(144,238,144);
    private boolean game_started;

    public ClientPanel(ClientBoard board) {
        this.board = board;
        tiles = new ArrayList<>();
        addMouseListener(new ClientPanelMouseListener(tiles, this));
    }

    @Override
    public void paintComponent(Graphics g) {
        tiles.clear();
        calculateScaling();
        calculateMargins();
        super.paintComponent(g);
        Graphics2D graphics = (Graphics2D) g;
        Tile[][] tiles_to_draw = board.getBoard();
        int sizeX = board.getBoardSizeX();
        int sizeY = board.getBoardSizeY();
        for (int i = 0; i<sizeX; i++) {
            for (int j = 0; j<sizeY; j++) {
                if (tiles_to_draw[j][i] != null) {
                    Tile tile = tiles_to_draw[j][i];
                    Point center_point = transformTileCoordinates(i, j);
                    drawTile(tile, graphics);
                    if (tile.getPawn() != null) {
                        Color color = tile.getPawn().getColor();
                        drawPawn(center_point.x, center_point.y, radius, graphics, color);
                    }
                }
            }
        }
        if(active_tile != null)
            drawActiveTile(active_tile, graphics);
        if(available_tiles != null)
            drawAvailableTiles(graphics);
    }

    public Point transformTileCoordinates(int x, int y) {
        int size = 2 * radius + spacing;
        int transformedQ = (int) ( marginX + size * ( x + 1.0/2 * y) );
        int transformedR = (int) ( marginY + size * ( Math.sqrt(3) / 2 * y ) );
        return new Point(transformedQ, transformedR);
    }

    private void drawTile(Tile tile, Graphics2D g) {
        Color color = tile.getColor();
        Point center_point = transformTileCoordinates(tile.getX(), tile.getY());
        int centerX = center_point.x;
        int centerY = center_point.y;
        g.setPaint(color);
        g.fillOval(centerX - radius-1, centerY - radius-1, 2*radius+2, 2*radius+2);
        Ellipse2D.Double circle = new Ellipse2D.Double(centerX - radius-1, centerY - radius-1, 2*radius+2, 2*radius+2);
        tile.setCircle(circle);
        tiles.add(tile);
        g.setPaint(Color.LIGHT_GRAY);
        g.fillOval(centerX - radius+1, centerY - radius+1, 2*radius-2, 2*radius-2);
    }

    private void drawPawn(int centerX, int centerY, int size, Graphics2D g, Color color){
        g.setPaint(color);
        g.fillOval(centerX - size+1, centerY - size+1, 2*size-2, 2*size-2);
    }

    private void drawActiveTile(Tile tile, Graphics2D g) {
        Point center_point = transformTileCoordinates(tile.getX(), tile.getY());
        int centerX = center_point.x;
        int centerY = center_point.y;
        g.setPaint(Color.BLACK);
        g.drawOval(centerX - radius-1, centerY - radius-1, 2*radius+2, 2*radius+2);
    }

    private void drawAvailableTiles(Graphics2D g) {
        for (Point point : available_tiles) {
            Tile tile = new Tile(point.x, point.y, available_color);
            drawAvailableTile(tile, g);
        }
    }

    private void drawAvailableTile(Tile tile, Graphics2D g) {
        Color color = tile.getColor();
        Point center_point = transformTileCoordinates(tile.getX(), tile.getY());
        int centerX = center_point.x;
        int centerY = center_point.y;
        g.setPaint(color);
        g.fillOval(centerX - radius-1, centerY - radius-1, 2*radius+2, 2*radius+2);
        Ellipse2D.Double circle = new Ellipse2D.Double(centerX - radius-1, centerY - radius-1, 2*radius+2, 2*radius+2);
        tile.setCircle(circle);
        tiles.add(tile);
    }

    public Point calculateMargins() {
        int index_x = (board.getBoardSizeX() - 1) / 2;
        int index_y = (board.getBoardSizeY() - 1) / 2;
        int half_panel_width = getWidth() / 2;
        int half_panel_height = getHeight() / 2;

        int size = 2 * radius + spacing;
        int transformedQ = (int) (size * ( index_x + 1.0/2 * index_y) );
        int transformedR = (int) (size * ( Math.sqrt(3) / 2 * index_y ) );

        marginX = half_panel_width - transformedQ;
        marginY = half_panel_height - transformedR;
        return new Point(marginX, marginY);
    }

    public int calculateScaling() {
        int panel_width = getWidth();
        int panel_height = getHeight();

        int number_of_rows = board.getBoardSizeY() - 4;
        int number_of_columns = ((board.getBoardSizeX() - 5) / 4) * 3 + 1;

        int max_spacing_y = (int) (panel_height/(number_of_rows * (3.0 * Math.sqrt(3))));
        int max_spacing_x = (int) (panel_width/ (number_of_columns * (4.0 * Math.sqrt(3))));

        spacing = Math.min(max_spacing_x, max_spacing_y) +1;
        radius = (int) (2 * spacing)+1;
        return radius;
    }

    public void setClickedTile(Tile tile) {
        if (isGameStarted()) {
            //nie kliknelismy w zadne pole
            if (tile == null) {
                active_tile = null;
                available_tiles = null;
            }
            //klikniecie pionka do poruszenia sie
            else if (active_tile == null && tile.containsPawn()) {
                String message = ClientEncoder.createMovesMessage(tile.getX(), tile.getY());
                ClientApplication.sendMessageToServer(message);
                active_tile = tile;
            } else if (active_tile != null) {
                //klikniecie pola do ruszenia sie na nie pionkiem
                if (!tile.containsPawn()) {
                    int fromX = active_tile.getX();
                    int fromY = active_tile.getY();
                    int toX = tile.getX();
                    int toY = tile.getY();
                    String message = ClientEncoder.createMoveMessage(fromX, fromY, toX, toY);
                    ClientApplication.sendMessageToServer(message);
                    active_tile = null;
                    available_tiles = null;
                }
                //zmiana aktywnego pionka
                else {
                    String message = ClientEncoder.createMovesMessage(tile.getX(), tile.getY());
                    ClientApplication.sendMessageToServer(message);
                    active_tile = tile;
                }
            }
            repaint();
        }
    }

    public ClientBoard getBoard() {
        return board;
    }

    public void setAvailableTiles(ArrayList<Point> available_tiles) {
        this.available_tiles = available_tiles;
    }

    public void startGame() {
        game_started = true;
    }

    public boolean isGameStarted() {
        return game_started;
    }
}
