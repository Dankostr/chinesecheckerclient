package com.app.gui;

import com.app.ClientApplication;
import com.app.gameobjects.Tile;
import com.app.server.ClientEncoder;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

public class ClientPanelMouseListener extends MouseAdapter {

    private ArrayList<Tile> tiles;
    private ClientPanel panel;

    ClientPanelMouseListener(ArrayList<Tile> tiles, ClientPanel panel) {
        this.tiles = tiles;
        this.panel = panel;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        for (Tile tile : tiles) {
            Ellipse2D circle = tile.getCircle();
            if (circle.contains(e.getPoint())) {
                panel.setClickedTile(tile);
                System.out.println("Kliknieto pole do gry o wspolrzednych "+tile.getX()+" "+tile.getY());
                return;
            }
        }
        System.out.println("Klikniety punkt: "+e.getPoint().getX()+" "+e.getPoint().getY());
        panel.setClickedTile(null);
    }
}
