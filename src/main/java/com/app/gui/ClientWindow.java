package com.app.gui;

import com.app.server.ClientConnector;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class ClientWindow extends JFrame {

    private String playerName;
    private ClientPanel boardPanel;
    private JLabel observersLabel;
    private ArrayList<JLabel> winnerLabels;
    private ArrayList<JButton> playerButtons;
    private ArrayList<JButton> botButtons;
    private JTextField informationTextField;
    private Color[] colors;
    private JButton skipTurnButton;
    private JButton startGameButton;
    private boolean game_started;

    public ClientWindow() {
        colors = new Color[6];
        colors[0] = new Color(255, 0, 0);
        colors[2] = new Color(0,0, 255);
        colors[4] = new Color(0, 255, 0);
        colors[5] = new Color(0, 255, 255);
        colors[3] = new Color(255, 255, 0);
        colors[1] = new Color(255, 0, 255);
    }

    public void updateObservers(String observers) {
        observersLabel.setText(observers);
    }

    public ClientPanel getClientPanel() {
        return boardPanel;
    }

    public void setClientPanel(ClientPanel panel) {
        this.boardPanel = panel;
    }

    public void setObserversLabel(JLabel label) {
        this.observersLabel = label;
    }

    public void setWinnerLabels(ArrayList<JLabel> winnerLabels) {
        this.winnerLabels = winnerLabels;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerButtons(ArrayList<JButton> playerButtons) {
        this.playerButtons = playerButtons;
    }

    public void setBotButtons(ArrayList<JButton> botButtons) {
        this.botButtons = botButtons;
    }

    public void setSkipTurnButton(JButton button) {
        this.skipTurnButton = button;
    }

    public void setStartGameButton(JButton button) {
        this.startGameButton = button;
    }

    public void setInformationTextField(JTextField informationTextField) {
        this.informationTextField = informationTextField;
    }

    public boolean isGameStarted() {
        return game_started;
    }

    public void setPlayer(int slot, String player) {
        JButton player_button = playerButtons.get(slot);
        JButton bot_button = botButtons.get(slot);
        player_button.setText(player);
        player_button.setEnabled(false);
        if (playerName.equals(player)){
            bot_button.setText("Obserwuj gre");
            bot_button.setEnabled(true);
        }
        else {
            bot_button.setText("");
            bot_button.setEnabled(false);
        }
        repaint();
    }

    public void setBot(int slot) {
        JButton player_button = playerButtons.get(slot);
        JButton bot_button = botButtons.get(slot);
        player_button.setText("Bot");
        bot_button.setText("Usun bota");
        player_button.setEnabled(false);
        bot_button.setEnabled(true);
        bot_button.setForeground(colors[slot]);
        repaint();
    }

    public void removePlayer(int slot) {
        JButton player_button = playerButtons.get(slot);
        JButton bot_button = botButtons.get(slot);
        player_button.setText("Dolacz do gry");
        bot_button.setText("Dodaj bota");
        player_button.setEnabled(true);
        bot_button.setEnabled(true);
        repaint();
    }

    public void setMessage(String message){
        informationTextField.setText(message);
    }

    public void startGame() {
        game_started = true;
        boardPanel.startGame();
        setMessage("Gra rozpoczeta!");
        startGameButton.setText("");
        startGameButton.setEnabled(false);
        for (int i=0; i<6; i++) {
            playerButtons.get(i).setEnabled(false);
            botButtons.get(i).setEnabled(false);
            winnerLabels.get(i).setText("");
        }
    }

    public void updateListOfWinners(int number_of_winners, String[] winners) {
        for (int i=0; i<number_of_winners; i++) {
            winnerLabels.get(i).setText((i+1)+". "+winners[i]);
        }
    }

    public void setPlayerTurn() {
        setMessage("Twoja tura");
        skipTurnButton.setEnabled(true);
    }

    public void setEnemyTurn() {
        setMessage("Tura przeciwnika");
        skipTurnButton.setEnabled(false);
    }

    public void resetStartGameButton() {
        startGameButton.setEnabled(true);
        startGameButton.setText("Rozpocznij gre");
    }

    public void resetComponents() {
        startGameButton.setText("Rozpocznij gre");
        startGameButton.setEnabled(true);
        boardPanel.getBoard().generateBoard();
        for (int i=0; i<6; i++) {
            playerButtons.get(i).setEnabled(true);
            playerButtons.get(i).setText("Dolacz do gry");
            botButtons.get(i).setEnabled(true);
            botButtons.get(i).setText("Dodaj bota");
        }
        skipTurnButton.setEnabled(false);
        repaint();
    }


}
