package com.app.gui;

import com.app.ClientApplication;
import com.app.gui.builder.GuiDirector;
import com.app.server.ClientConnector;
import com.app.server.ClientEncoder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class NickEntryWindow extends JFrame {

    private JTextField nick_textfield;
    private JTextField ip_textfield;

    public NickEntryWindow() {
        setTitle("Chinese Checkers");
        setBounds(500, 300, 400, 150);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        createGuiElements();
    }

    private void createGuiElements() {
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = new GridBagConstraints();

        constraints.weightx = 0.2;
        constraints.weighty = 0.5;
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 0;
        constraints.gridy = 0;
        add(new JLabel("Podaj nick: "), constraints);


        constraints.weightx = 0.7;
        constraints.gridx = 1;
        nick_textfield = new JTextField("");
        add(nick_textfield, constraints);

        constraints.weightx = 0.1;
        constraints.gridx = 2;
        constraints.gridheight = 2;
        JButton button = new JButton("Rozpocznij gre");
        button.addActionListener(getButtonListener());
        add(button, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        constraints.weightx = 0.2;
        constraints.gridheight = 1;
        add(new JLabel("Podaj adres IP: "), constraints);

        constraints.weightx = 0.7;
        constraints.gridx = 1;
        ip_textfield = new JTextField("127.0.0.1");
        add(ip_textfield, constraints);
    }

    public void setNick(String nick) {
        nick_textfield.setText(nick);
    }

    public String getNick() throws IncorrectNickException {
        String nick = nick_textfield.getText();
        int len = nick.length();
        if(len >0 && len <= 30) {
            return nick;
        }
        else {
            throw new IncorrectNickException();
        }
    }

    public void verifyNick() {
        try {
            String nick = getNick();
            String message = ClientEncoder.createNickMessage(nick);
            ClientApplication.sendMessageToServer(message);
        }
        catch(IncorrectNickException ex) {
            setNick("");
        }
    }

    public void acceptNick(String nick) {
        createClientWindow(nick);
    }

    public void rejectNick() {
        setNick("Podany przez uzytkownika nick jest zajety");
    }

    private void createClientWindow(String nick) {
        GuiDirector builder = new GuiDirector();
        builder.constructGui();
        ClientWindow window = builder.getWindow();
        window.setPlayerName(nick);
        try {
            ClientApplication.sendMessageToServer(ClientEncoder.createGetCurrentStateMessage());
        }
        catch (Exception ex) {

        }
        window.setVisible(true);
        this.setVisible(false);

        window.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent windowEvent) {
                ClientApplication.sendMessageToServer(ClientEncoder.createPlayerLeftGameMessage());
            }
        });

    }


    public void connectToServer() {
        try {
            ClientConnector connector = new ClientConnector(ip_textfield.getText());
            ClientApplication.setClientConnector(connector);
        }
        catch (Exception ex) {
            ip_textfield.setText("Blad polaczenia");
        }
    }

    public ActionListener getButtonListener() {
        return new ButtonListener();
    }

    class ButtonListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            connectToServer();
            verifyNick();
        }
    }

}
