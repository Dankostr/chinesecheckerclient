package com.app.gui;

import com.app.ClientApplication;
import com.app.server.ClientEncoder;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PlayerButtonActionListener implements ActionListener {

    private int slot;
    private JButton button;

    public PlayerButtonActionListener(int slot, JButton button) {
        this.slot = slot - 1;
        this.button = button;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String message = ClientEncoder.createJoinSlotMessage(slot);
        ClientApplication.sendMessageToServer(message);
    }

    public int getSlot() {
        return slot;
    }

    public JButton getButton() {
        return button;
    }
}
