package com.app.gui.builder;

import com.app.ClientApplication;
import com.app.gui.BotButtonActionListener;
import com.app.gui.ClientPanel;
import com.app.gui.ClientWindow;
import com.app.gui.PlayerButtonActionListener;
import com.app.server.ClientEncoder;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class FrameGuiBuilder extends GuiBuilder {

    private GridBagConstraints constraints;
    private ClientPanel panel;
    private ClientWindow window;
    private Color[] colors;

    FrameGuiBuilder(ClientPanel panel) {
        this.panel = panel;
        colors = new Color[6];
        colors[0] = new Color(255, 0, 0);
        colors[2] = new Color(0,0, 255);
        colors[4] = new Color(0, 255, 0);
        colors[5] = new Color(0, 255, 255);
        colors[3] = new Color(255, 255, 0);
        colors[1] = new Color(255, 0, 255);
    }

    @Override
    public void createNewComponent() {
        window = new ClientWindow();
        component = window;
    }

    @Override
    public void buildComponent() {
        setBasicOptions();
        addPanel();
        addObservers();
        addPlayerButtons();
        addBotButtons();
        addListOfWinners();
        addStartGameAndSkipTurnButton();
        addDummyPanels();
    }

    public void setBasicOptions() {
        component.setFont(new Font("TYPE1_FONT", Font.PLAIN ,15));
        component.setBackground(Color.GRAY);
        component.setBounds(100, 50, 1024, 768);
        component.setName("Chinese checkers");
        window.setTitle("Chinese checkers");
        window.setResizable(false);
        window.getContentPane().setLayout(new GridBagLayout());
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
    }

    public void addPanel() {
        constraints.gridx = 1;
        constraints.gridy = 1;
        constraints.gridwidth = 15;
        constraints.gridheight = 19;
        constraints.weightx = 0.9;
        constraints.weighty = 0.9;
        window.setClientPanel(panel);
        window.add(panel, constraints);
    }

    public void addObservers() {
        constraints.weightx = 0.1;
        constraints.weighty = 0.1;
        JLabel observers = new JLabel("...");
        window.setObserversLabel(observers);

        constraints.gridx = 1;
        constraints.gridy = 20;
        constraints.gridwidth = 15;
        constraints.gridheight = 1;
        JTextField textField = new JTextField("Witamy w grze Chinese Checkers");
        textField.setEditable(false);
        window.add(textField, constraints);
        window.setInformationTextField(textField);
    }

    public void addPlayerButtons() {
        ArrayList<JButton> buttons = new ArrayList<>();
        constraints.gridx = 16;
        constraints.gridy = 1;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        constraints.weighty = 0.1;
        constraints.weightx = 0.1;
        window.add(new JLabel("Gracze: "), constraints);
        constraints.gridy = 2;
        for (int i = 1; i<=6; i++) {
            JButton button = new JButton("Dolacz do gry");
            button.setForeground(colors[i-1]);
            button.addActionListener(new PlayerButtonActionListener(i , button));
            window.add(button, constraints);
            buttons.add(button);
            constraints.gridy++;
        }
        window.setPlayerButtons(buttons);
    }

    public void addBotButtons() {
        ArrayList<JButton> buttons = new ArrayList<>();
        constraints.gridx = 17;
        constraints.gridy = 2;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        for (int i = 1; i<=6; i++) {
            JButton button = new JButton("Dodaj bota");
            button.setForeground(colors[i-1]);
            button.addActionListener(new BotButtonActionListener(i, button));
            window.add(button, constraints);
            buttons.add(button);
            constraints.gridy++;
        }
        window.setBotButtons(buttons);
    }

    public void addListOfWinners() {
        ArrayList<JLabel> labels = new ArrayList<>();
        constraints.gridx = 16;
        constraints.gridy = 8;
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        window.add(new JLabel("Ranking: "), constraints);
        constraints.gridy = 9;
        for (int i = 1; i<=6; i++) {
            String string = "" + i + ".";
            JLabel label = new JLabel(string);
            window.add(label, constraints);
            labels.add(label);
            constraints.gridy++;
        }
        window.setWinnerLabels(labels);
    }

    public void addStartGameAndSkipTurnButton() {
        JButton startGameButton = new JButton("Rozpocznij gre");
        startGameButton.addActionListener(new StartGameButtonActionListener(startGameButton));
        constraints.gridx = 16;
        constraints.gridy= 16;
        constraints.gridheight = 2;
        constraints.gridwidth = 2;
        window.add(startGameButton, constraints);
        window.setStartGameButton(startGameButton);

        JButton skipTurnButton = new JButton("Pomin ture");
        skipTurnButton.addActionListener(new SkipTurnButtonActionListener());
        skipTurnButton.setEnabled(false);
        constraints.gridx = 16;
        constraints.gridy = 18;
        constraints.gridheight = 1;
        constraints.gridwidth = 1;
        window.add(skipTurnButton, constraints);
        window.setSkipTurnButton(skipTurnButton);
    }

    // x: 0 - 20   y: 0 - 21
    public void addDummyPanels() {
        constraints.gridwidth = 1;
        constraints.gridheight = 1;
        for (int i=0; i<=21; i++) {
            constraints.gridx = 0;
            constraints.gridy = i;
            window.add(new JPanel(), constraints);

            constraints.gridx = 20;
            constraints.gridy = i;
            window.add(new JPanel(), constraints);
        }

        for (int i=0; i<=21; i++) {
            constraints.gridx = i;
            constraints.gridy = 0;
            window.add(new JPanel(), constraints);

            constraints.gridx = i;
            constraints.gridy = 23;
            window.add(new JPanel(), constraints);
        }
    }

    class SkipTurnButtonActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String message = ClientEncoder.createSkipTurnMessage();
            ClientApplication.sendMessageToServer(message);
        }
    }

    class StartGameButtonActionListener implements ActionListener {

        private JButton button;

        StartGameButtonActionListener(JButton button) {
            this.button = button;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (button.getText().equals("Rozpocznij gre")) {
                String message = ClientEncoder.createStartGameMessage();
                ClientApplication.sendMessageToServer(message);
                button.setText("Zrezygnuj z gry");
                button.setEnabled(true);
                window.setMessage("Zgloszono gotowosc do gry");
            }
            else if (button.getText().equals("Zrezygnuj z gry")) {
                String message = ClientEncoder.createPlayerNotReadyMessage();
                ClientApplication.sendMessageToServer(message);
                button.setText("Rozpocznij gre");
                button.setEnabled(true);
                window.setMessage("Zgloszono brak gotowosci do gry");
            }
        }
    }

}
