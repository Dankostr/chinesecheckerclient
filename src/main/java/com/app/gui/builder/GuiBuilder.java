package com.app.gui.builder;

import java.awt.*;

public abstract class GuiBuilder {

    protected Component component;

    public Component getComponent() {
        return component;
    }

    public abstract void createNewComponent();

    public abstract void buildComponent();
}
