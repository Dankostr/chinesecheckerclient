package com.app.gui.builder;

import com.app.gui.ClientPanel;
import com.app.gui.ClientWindow;
import com.app.server.EventHandler;

import java.awt.*;

public class GuiDirector {

    private GuiBuilder builder;
    private ClientWindow window;

    private void setBuilder(GuiBuilder builder) {
        this.builder = builder;
    }

    private Component getComponent() {
        return builder.getComponent();
    }

    public ClientWindow getWindow() {
        return window;
    }

    public void constructGui() {
        setBuilder(new PanelGuiBuilder());
        builder.createNewComponent();
        builder.buildComponent();
        ClientPanel panel = (ClientPanel) getComponent();
        EventHandler.setPanel(panel);

        setBuilder(new FrameGuiBuilder(panel));
        builder.createNewComponent();
        builder.buildComponent();
        this.window = (ClientWindow) getComponent();

        window.setClientPanel(panel);
        window.repaint();

        EventHandler.setWindow(window);
    }
}
