package com.app.gui.builder;

import com.app.gameobjects.ClientBoard;
import com.app.gui.ClientPanel;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanelGuiBuilder extends GuiBuilder {

    @Override
    public void createNewComponent() {
        ClientBoard board = new ClientBoard(4);
        this.component = new ClientPanel(board);
    }

    @Override
    public void buildComponent() {
        component.setBackground(Color.WHITE);
    }
}
