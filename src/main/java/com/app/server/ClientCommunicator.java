package com.app.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientCommunicator implements Runnable {

    private Socket clientSocket;
    private BufferedReader clientInput;
    private PrintWriter clientOutput;

    public ClientCommunicator(Socket clientSocket){
        this.clientSocket=clientSocket;
        try {
            clientInput=new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            clientOutput=new PrintWriter(clientSocket.getOutputStream(),true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    synchronized public void run() {
        while(true) {
            try {
                waitForMessage();
                decodeMessage(readMessage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void waitForMessage() throws IOException {
        while(!clientInput.ready()) {
        }
    }

    private String readMessage() throws IOException {
        return clientInput.readLine();
    }

    private void decodeMessage(String message) {
        System.out.println("Message: "+message);
        ClientDecoder.decodeMessage(message);
    }

    protected void sendMessage(String message) {
        clientOutput.println(message);
    }

}
