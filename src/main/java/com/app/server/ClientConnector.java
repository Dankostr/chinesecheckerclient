package com.app.server;

import java.io.IOException;
import java.net.Socket;

public class ClientConnector {

    private final static int PORT_NUMBER=2137;
    private Socket socket;
    private Thread thread;
    private ClientCommunicator communicator;

    public ClientConnector(){
        this("127.0.0.1");
    }

    public ClientConnector(String ipadress) {
        try {
            socket=new Socket(ipadress,PORT_NUMBER);
        } catch (IOException e) {
            System.out.println("Connection error");
        }
        communicator = new ClientCommunicator(socket);
        thread= new Thread(communicator);
        thread.start();
    }

    public void sendMessage(String message) {
        communicator.sendMessage(message);
    }
}