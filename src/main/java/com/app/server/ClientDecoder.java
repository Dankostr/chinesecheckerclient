package com.app.server;

import java.awt.*;
import java.util.ArrayList;

public class ClientDecoder {

    /**
     * Message format OPERATION_NAME ADDITIONAL_ARGUMENTS
     */
    public static String decodeMessage(String message) {
        if (message.startsWith("ALL COMMANDS")) {
            String arr[] = message.split("\n", 2);
            if(arr.length > 1) {
                return decodeAllServerCommands(arr[1]);
            }
            else {
                return "NO COMMANDS WERE SENT";
            }
        }
        String arr[] = message.split(" ", 2);
        String command = arr[0];
        String arguments;
        if(arr.length > 1) {
             arguments = arr[1];
        }
        else {
            arguments = null;
        }
        if(command.equals("NICK_ACCEPTED")) {
            return decodeNickAcceptedCommand(arguments);
        }
        else if(command.equals("NICK_REJECTED")) {
            return decodeNickRejectedCommand(arguments);
        }
        else if(command.equals("MOVE_DONE")) {
            return decodeMoveCommand(arguments);
        }
        else if(command.equals("OBSERVERS")) {
            return decodeObserversCommand(arguments);
        }
        else if(command.equals("AVAILABLE_MOVES")) {
            return decodeAvailableMovesCommand(arguments);
        }
        else if(command.equals("SLOT_JOIN")) {
            return decodeJoinSlotCommand(arguments);
        }
        else if(command.equals("PLAYER_JOINED")) {
            return decodePlayerJoinedCommand(arguments);
        }
        else if(command.equals("BOT_ADDED")) {
            return decodeBotAddedCommand(arguments);
        }
        else if(command.equals("PLAYER_LEFT")) {
            return decodePlayerLeftCommand(arguments);
        }
        else if(command.equals("BOT_REMOVED")) {
            return decodeBotRemovedCommand(arguments);
        }
        else if(command.equals("GAME_READY")) {
            return decodeGameReadyCommand();
        }
        else if(command.equals("WINNERS")) {
            return decodeWinnersCommand(arguments);
        }
        else if(command.equals("YOUR_TURN")) {
            return decodeYourTurnCommand();
        }
        else if(command.equals("ENEMY_TURN")) {
            return decodeEnemyTurnCommand();
        }
        else if(command.equals("GAME_INFO")) {
            return decodeGameInfoCommand(arguments);
        }
        else if(command.equals("DRAW")) {
            return decodeDrawCommand();
        }
        else if(command.equals("GAME_FINISHED")) {
            return decodeGameFinishedCommand();
        }
        return "UNKNOWN COMMAND";
    }

    private static String decodeAllServerCommands(String commands) {
        String arr[] = commands.split("\n");
        for (String str : arr) {
            decodeMessage(str);
        }
        return "DECODED "+arr.length+" MESSAGES";
    }


    private static String decodeNickAcceptedCommand(String arguments) {
        return EventHandler.acceptNick(arguments);
    }

    private static String decodeNickRejectedCommand(String arguments) {
        return EventHandler.rejectNick();
    }

    private static String decodeMoveCommand(String arguments) {
        String arr[] = arguments.split(" ");
        if(arr[0].equals("FAILURE")) {
            return EventHandler.notifyAboutWrongMove();
        }else if(! arr[0].equals("SUCCESS")){

            int fromX = Integer.parseInt(arr[0]);
            int fromY = Integer.parseInt(arr[1]);
            int toX = Integer.parseInt(arr[2]);
            int toY = Integer.parseInt(arr[3]);
            return EventHandler.movePawn(fromX, fromY, toX, toY);
        }
        return "MOVE SUCCESS";
    }

    private static String decodeObserversCommand(String arguments) {
        String arr[] = arguments.split(" ");
        String result = "";
        for (int i=0; i<arr.length - 1; i++) {
            result += arr[i];
            result += ", ";
        }
        result += arr[arr.length-1];
        return EventHandler.updateObservers(result);
    }

    private static String decodeAvailableMovesCommand(String arguments) {
        String arr[] = arguments.split(" ");
        int number_of_moves = Integer.parseInt(arr[0]);
        ArrayList<Point> moves = new ArrayList<>();
        for (int i = 0; i<number_of_moves; i++) {
            int x = Integer.parseInt(arr[1 + i*2]);
            int y = Integer.parseInt(arr[2 + i*2]);
            Point point = new Point(x, y);
            moves.add(point);
        }
        return EventHandler.setAvailableMoves(moves);
    }

    private static String decodeJoinSlotCommand(String arguments) {
        int slot = Integer.parseInt(arguments);
        if (slot != -1) {
            return EventHandler.joinSlot(slot );
        }
        return "SLOT IS OCCUPIED";
    }

    private static String decodePlayerJoinedCommand(String arguments) {
        String arr[] = arguments.split(" ");
        int slot = Integer.parseInt(arr[0]);
        String nick = arr[1];
        return EventHandler.addPlayerToSlot(slot, nick);
    }

    private static String decodePlayerLeftCommand(String arguments) {
        int slot = Integer.parseInt(arguments);
        return EventHandler.removePlayerFromSlot(slot);
    }

    private static String decodeBotAddedCommand(String arguments) {
        int slot = Integer.parseInt(arguments);
        return EventHandler.addBotToSlot(slot);
    }

    private static String decodeBotRemovedCommand(String arguments) {
        int slot = Integer.parseInt(arguments);
        return EventHandler.removeBotFromSlot(slot);
    }

    private static String decodeGameReadyCommand() {
        return EventHandler.setGameStateToReady();
    }

    private static String decodeWinnersCommand(String arguments) {
        String arr[] = arguments.split(" ");
        return EventHandler.setWinners(arr);
    }

    private static String decodeYourTurnCommand() {
        return EventHandler.setPlayersTurn();
    }

    private static String decodeEnemyTurnCommand() {
        return EventHandler.setEnemyTurn();
    }

    private static String decodeGameInfoCommand(String arguments) {
        String arr[] = arguments.split(" ", 2);
        if(!arr[0].equals("ERROR")) {
            int number_of_players = Integer.parseInt(arr[0]);
            String players = arr[1];
            return EventHandler.generateBoardForPlayers(number_of_players, players);
        }
        return EventHandler.generateBoardForPlayers(0,"ERROR");
    }

    private static String decodeDrawCommand() {
        return EventHandler.draw();
    }

    private static String decodeGameFinishedCommand() {
        return EventHandler.gameFinished();
    }

}
