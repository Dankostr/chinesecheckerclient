package com.app.server;

public class ClientEncoder {

    /**
     * Message format OPERATION_NAME ADDITIONAL_ARGUMENTS
     */
    public static String createNickMessage(String nick){
        return "NICK " + nick;
    }

    public static String createJoinSlotMessage(int slot_number) {
        return "JOINSLOT " +slot_number;
    }

    public static String createMoveMessage(int fromX, int fromY, int toX, int toY) {
        return "MOVE " + fromX + " " + fromY + " " + toX + " " + toY;
    }

    public static String createMovesMessage(int X, int Y) {
        return "MOVES " + X + " " + Y;
    }

    public static String createAddBotMessage(int slot_number) {
        return "ADD_BOT " + slot_number;
    }

    public static String createRemoveBotMessage(int slot_number) {
        return "REMOVE_BOT " + slot_number;
    }

    public static String createLeaveSlotMessage(int slot_number) {
        return "LEAVESLOT "+slot_number;
    }

    public static String createStartGameMessage() {
        return "PLAYER_READY";
    }

    public static String createSkipTurnMessage() {
        return "SKIP_TURN";
    }

    public static String createPlayerNotReadyMessage() {
        return "PLAYER_NOT_READY";
    }

    public static String createPlayerLeftGameMessage() {
        return "PLAYER_HAS_LEFT";
    }

    public static String createGetCurrentStateMessage() {
        return "GET_CURRENT_STATE";
    }
}
