package com.app.server;

import com.app.gameobjects.ClientBoard;
import com.app.gameobjects.Pawn;
import com.app.gameobjects.Tile;
import com.app.gui.ClientPanel;
import com.app.gui.ClientWindow;
import com.app.gui.NickEntryWindow;

import java.awt.*;
import java.util.ArrayList;

public class EventHandler {

    private static ClientPanel panel;
    private static ClientWindow window;
    private static NickEntryWindow nickEntryWindow;

    public static String acceptNick(String nick){
        nickEntryWindow.acceptNick(nick);
        return "NICK ACCEPTED "+nick;
    }

    public static String rejectNick() {
        nickEntryWindow.rejectNick();
        return "NICK REJECTED";
    }

    public static String movePawn(int fromX, int fromY, int toX, int toY) {
        ClientBoard board = panel.getBoard();
        Tile[][] tiles = board.getTileList();
        Pawn pawn = tiles[fromY][fromX].getPawn();
        tiles[fromY][fromX].setPawn(null);
        tiles[toY][toX].setPawn(pawn);
        panel.repaint();
        window.setMessage("Wykonano ruch");
        return "PAWN MOVED FROM ("+fromX+","+fromY+") TO ("+toX+","+toY+")";
    }

    public static String updateObservers(String observers) {
        window.updateObservers(observers);
        window.repaint();
        return "NEW LIST OF OBSERVERS: "+observers;
    }

    public static String joinSlot(int slot_number) {
        window.setPlayer(slot_number, window.getPlayerName());
        window.setMessage("Dolaczono do gry na pozycji "+(slot_number+1));
        return "PLAYER HAS JOINED "+slot_number+" SLOT";
    }

    public static String setAvailableMoves(ArrayList<Point> moves) {
        panel.setAvailableTiles(moves);
        return "THERE ARE "+moves.size()+" AVAILABLE MOVES";
    }

    public static String addPlayerToSlot(int slot_number, String player_name) {
        window.setPlayer(slot_number, player_name);
        window.setMessage("Do gry dolaczyl gracz "+player_name+" na pozycji "+(slot_number+1));
        return "PLAYER "+player_name+" HAS JOINED THE SLOT "+slot_number;
    }

    public static String removePlayerFromSlot(int slot_number) {
        window.removePlayer(slot_number);
        window.setMessage("Z pozycji "+slot_number+" odszedl gracz");
        return "PLAYER HAS LEFT THE SLOT "+slot_number;
    }

    public static String addBotToSlot(int slot_number) {
        window.setBot(slot_number);
        window.setMessage("Dodano bota na pozycji "+slot_number);
        return "BOT WAS ADDED TO SLOT "+slot_number;
    }

    public static String removeBotFromSlot(int slot_number) {
        window.removePlayer(slot_number);
        window.setMessage("Usunieto bota z pozycji "+slot_number);
        return "BOT WAS REMOVED FROM SLOT "+slot_number;
    }

    public static String setGameStateToReady() {
        window.startGame();
        return "ALL PLAYERS ARE READY";
    }

    public static String setWinners(String[] winners) {
        int number_of_winners = winners.length;
        window.updateListOfWinners(number_of_winners, winners);
        return "THERE ARE "+winners.length+" WINNERS";
    }

    public static String setPlayersTurn() {
        window.setPlayerTurn();
        return "PLAYERS TURN";
    }

    public static String setEnemyTurn() {
        window.setEnemyTurn();
        return "ENEMY TURN";
    }

    public static String generateBoardForPlayers(int number_of_players, String players) {
        if(players.equals("ERROR")) {
            window.setMessage("Zla liczba graczy badz ich zle ustawienie przy stole");
            window.resetStartGameButton();
            return "BOARD GENERATING ERROR";
        }
        String arr[] = players.split(" ");
        boolean[] players_array = new boolean[6];
        for (int i = 0; i<number_of_players; i++) {
            int player = Integer.parseInt(arr[i]);
            players_array[player] = true;
        }
        ClientBoard board = panel.getBoard();
        board.setPlayersArray(players_array);
        board.generateBoard();
        panel.repaint();
        return "GENERATED BOARD FOR "+number_of_players+" PLAYERS";
    }

    public static String notifyAboutWrongMove() {
        window.setMessage("Ten ruch jest nieprawidlowy");
        return "WRONG MOVE";
    }

    public static String draw() {
        window.resetComponents();
        window.setMessage("Gra zakonczona remisem");
        return "GAME RESULT IS DRAW";
    }

    public static String gameFinished() {
        window.resetComponents();
        window.setMessage("Gra zakonczona");
        return "GAME IS FINISHED";
    }

    public static void setNickEntryWindow(NickEntryWindow window) {
        nickEntryWindow = window;
    }

    public static void setPanel(ClientPanel client_panel) {
        panel = client_panel;
    }

    public static void setWindow(ClientWindow client_window) {
        window = client_window;
    }


}
