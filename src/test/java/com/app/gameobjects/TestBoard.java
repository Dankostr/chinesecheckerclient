package com.app.gameobjects;

import org.junit.Assert;
import org.junit.Test;

public class TestBoard {

    @Test
    public void testBoardWithNormalSize() {
        ClientBoard board = new ClientBoard(4);
        Assert.assertEquals(21, board.getBoardSizeX());
        Assert.assertEquals(21, board.getBoardSizeY());

        for (int i = 0; i < board.getBoardSizeX(); i++) {
            Assert.assertTrue(board.getBoard()[0][i] == null);
        }

        for (int i = 0; i < board.getBoardSizeY(); i++) {
            Assert.assertTrue(board.getBoard()[i][0] == null);
        }

        Assert.assertTrue(board.getBoard().length == board.getBoardSizeY());
    }

    @Test
    public void testBoardWithDifferentSize() {
        ClientBoard board = new ClientBoard(1);
        Assert.assertEquals(9, board.getBoardSizeY());
        Assert.assertEquals(9, board.getBoardSizeX());

        board = new ClientBoard(6);
        boolean[] array = {true, true, true, true, true, true};
        board.setPlayersArray(array);
        Assert.assertEquals(29, board.getBoardSizeY());
        Assert.assertEquals(29, board.getBoardSizeX());
    }
}
