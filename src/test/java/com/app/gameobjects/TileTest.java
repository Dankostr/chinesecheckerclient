package com.app.gameobjects;

import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class TileTest {

    @Test
    public void testTile() {
        Tile test_tile = new Tile(0 ,0, Color.RED);
        Assert.assertEquals(test_tile.getColor(), Color.RED);

        test_tile = new Tile(4 , 5, Color.YELLOW);
        Assert.assertEquals(test_tile.getColor(), Color.YELLOW);

        Ellipse2D circle = new Ellipse2D.Double(50, 25, 100, 100);
        test_tile.setCircle(circle);
        Assert.assertEquals(test_tile.getCircle(), circle);
    }

    @Test
    public void testPawn() {
        Tile test_tile = new Tile(2, 10, Color.BLACK);
        Assert.assertTrue(!test_tile.containsPawn());

        test_tile.setPawn(new Pawn(Color.WHITE));
        Assert.assertNotNull(test_tile.getPawn());
        Assert.assertTrue(test_tile.containsPawn());
    }
}
