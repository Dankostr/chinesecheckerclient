package com.app.gui;

import org.junit.Assert;
import org.junit.Test;

import javax.swing.*;

public class ButtonsTest {

    @Test
    public void testPlayerButtonActionListener() {
        JButton button = new JButton();
        PlayerButtonActionListener listener = new PlayerButtonActionListener(3, button);
        Assert.assertTrue(listener.getSlot() == 2);
        Assert.assertTrue(listener.getButton() == button);
    }
}
