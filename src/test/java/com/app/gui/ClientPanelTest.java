package com.app.gui;

import com.app.gameobjects.ClientBoard;
import org.junit.Assert;
import org.junit.Test;

public class ClientPanelTest {

    @Test
    public void testTransformTileCoordinates() {
        ClientBoard test_board = new ClientBoard(4);
        ClientPanel panel = new ClientPanel(test_board);
        Assert.assertEquals(0, panel.transformTileCoordinates(0, 0).getX(), 0.01);
        Assert.assertEquals(0, panel.transformTileCoordinates(0, 0).getY(), 0.01);
    }
}
