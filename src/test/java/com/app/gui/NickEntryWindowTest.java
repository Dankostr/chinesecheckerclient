package com.app.gui;

import org.junit.Assert;
import org.junit.Test;

import java.awt.*;
import java.awt.event.ActionListener;

import static junit.framework.TestCase.fail;

public class NickEntryWindowTest {

    @Test
    public void testCorrectNick() {
        NickEntryWindow testWindow = new NickEntryWindow();
        String wynik;

        testWindow.setNick("Daniel");
        try {
            wynik = testWindow.getNick();
        }
        catch (IncorrectNickException ex) {
            wynik = ex.getMessage();
        }
        Assert.assertEquals("Daniel", wynik);

        testWindow.setNick("");
        try {
            wynik = testWindow.getNick();
        }
        catch (IncorrectNickException ex) {
            wynik = ex.getMessage();
        }
        Assert.assertEquals("Podano bledny nick", wynik);

        testWindow.setNick("ASDASdfgkfdasdjajffbjkdzmvfsdjkfncbjsd");
        try {
            wynik = testWindow.getNick();
        }
        catch (IncorrectNickException ex) {
            wynik = ex.getMessage();
        }
        Assert.assertEquals("Podano bledny nick", wynik);
    }

    @Test
    public void testButtonListener() {
        NickEntryWindow testWindow = new NickEntryWindow();
        ActionListener listener = testWindow.getButtonListener();
        Assert.assertTrue(listener instanceof NickEntryWindow.ButtonListener);
    }

    @Test
    public void testNickVerification() {
        NickEntryWindow testWindow = new NickEntryWindow();
        testWindow.setNick("sdkfmdsjnsadsadsafgkndkbknbfkbnkdfgndg");
        testWindow.verifyNick();
        try {
            testWindow.getNick();
            fail();
        }
        catch(IncorrectNickException ex) {
            Assert.assertEquals("Podano bledny nick", ex.getMessage());
        }
    }
}
