package com.app.gui.builder;

import com.app.gameobjects.ClientBoard;
import com.app.gui.ClientPanel;
import com.app.gui.ClientWindow;
import org.junit.Assert;
import org.junit.Test;

import java.awt.*;

import static org.junit.Assert.fail;

public class BuilderTest {

    @Test
    public void testPanelGuiBuilder() {
        PanelGuiBuilder panelGuiBuilder = new PanelGuiBuilder();
        panelGuiBuilder.createNewComponent();
        panelGuiBuilder.buildComponent();
        Assert.assertTrue(panelGuiBuilder.getComponent() != null);
        Assert.assertTrue(panelGuiBuilder.getComponent() instanceof ClientPanel);

        ClientPanel resultPanel = (ClientPanel) panelGuiBuilder.getComponent();
        Assert.assertTrue(resultPanel.getBackground() == Color.WHITE);
    }

    @Test
    public void testFrameGuiBuilder() {
        ClientBoard dummyBoard = new ClientBoard(4);
        FrameGuiBuilder frameGuiBuilder = new FrameGuiBuilder(new ClientPanel(dummyBoard));
        frameGuiBuilder.createNewComponent();
        frameGuiBuilder.buildComponent();
        Assert.assertTrue(frameGuiBuilder.getComponent() != null);
        Assert.assertTrue(frameGuiBuilder.getComponent() instanceof ClientWindow);
        ClientWindow window = (ClientWindow) frameGuiBuilder.getComponent();
        Assert.assertEquals("Chinese checkers", frameGuiBuilder.getComponent().getName());
        Assert.assertTrue(window.getContentPane().getLayout() instanceof GridBagLayout);
        Assert.assertTrue(window.isBackgroundSet());
        Assert.assertTrue(window.isFontSet());
    }

    @Test
    public void testGuiDirector() {
        GuiDirector director = new GuiDirector();
        try {
            director.constructGui();
        }
        catch (NullPointerException ex) {
            fail();
        }
        Assert.assertTrue(director.getWindow() != null);
        Assert.assertTrue(director.getWindow().getClientPanel() != null);
    }
}
