package com.app.server;

import com.app.gameobjects.ClientBoard;
import com.app.gui.ClientPanel;
import com.app.gui.ClientWindow;
import com.app.gui.NickEntryWindow;
import com.app.gui.builder.GuiBuilder;
import com.app.gui.builder.GuiDirector;
import org.junit.Assert;
import org.junit.Test;

import javax.swing.*;

public class ClientDecoderTest {

    public void setUpEventHandler() {
        GuiDirector builder = new GuiDirector();
        builder.constructGui();
        ClientWindow window = builder.getWindow();
        window.setPlayerName("testname");
        EventHandler.setNickEntryWindow(new NickEntryWindow());
    }

    @Test
    public void testNickMessages() {
        setUpEventHandler();
        String message1 = ClientDecoder.decodeMessage("NICK_ACCEPTED gracz");
        String message2 = ClientDecoder.decodeMessage("NICK_REJECTED gracz2");
        Assert.assertEquals("NICK ACCEPTED gracz", message1);
        Assert.assertEquals("NICK REJECTED",message2);
    }

    @Test
    public void testMoves() {
        setUpEventHandler();
        String message1 = ClientDecoder.decodeMessage("MOVE_DONE 9 6 10 6");
        String message2 = ClientDecoder.decodeMessage("MOVE_DONE 15 6 16 6");
        String message3 = ClientDecoder.decodeMessage("MOVE_DONE FAILURE");
        String message4 = ClientDecoder.decodeMessage("AVAILABLE_MOVES 3 10 11 11 10 11 11");
        Assert.assertEquals("PAWN MOVED FROM (9,6) TO (10,6)", message1);
        Assert.assertEquals("PAWN MOVED FROM (15,6) TO (16,6)",message2);
        Assert.assertEquals("WRONG MOVE", message3);
        Assert.assertEquals("THERE ARE 3 AVAILABLE MOVES", message4);
        String message5 = ClientDecoder.decodeMessage("MOVE_DONE SUCCESS");
        Assert.assertEquals("MOVE SUCCESS", message5);
    }

    @Test
    public void testObservers() {
        setUpEventHandler();
        String message1 = ClientDecoder.decodeMessage("OBSERVERS obs1 obs2 obs3");
        String message2 = ClientDecoder.decodeMessage("OBSERVERS spectator");
        Assert.assertEquals("NEW LIST OF OBSERVERS: obs1, obs2, obs3", message1);
        Assert.assertEquals("NEW LIST OF OBSERVERS: spectator", message2);
    }

    @Test
    public void testBotCommands() {
        setUpEventHandler();
        String message1 = ClientDecoder.decodeMessage("BOT_ADDED 1");
        String message2 = ClientDecoder.decodeMessage("BOT_REMOVED 2");
        Assert.assertEquals("BOT WAS ADDED TO SLOT 1", message1);
        Assert.assertEquals("BOT WAS REMOVED FROM SLOT 2", message2);
    }

    @Test
    public void testAllCommands() {
        setUpEventHandler();
        String message1 = ClientDecoder.decodeMessage("ALL COMMANDS\n");
        Assert.assertEquals("DECODED 1 MESSAGES", message1);
        String message2 = "ALL COMMANDS\nPLAYER_JOINED 5 Daniel\nSLOT_JOIN 0\nPLAYER_LEFT 3";
        String message3 = ClientDecoder.decodeMessage(message2);
        Assert.assertEquals("DECODED 3 MESSAGES",message3);
    }

    @Test
    public void testGameStatesCommands() {
        setUpEventHandler();
        String message1 = ClientDecoder.decodeMessage("GAME_READY");
        String message2 = ClientDecoder.decodeMessage("DRAW");
        String message3 = ClientDecoder.decodeMessage("GAME_FINISHED");
        Assert.assertEquals("ALL PLAYERS ARE READY",message1);
        Assert.assertEquals("GAME RESULT IS DRAW", message2);
        Assert.assertEquals("GAME IS FINISHED", message3);
    }

    @Test
    public void testWinnersCommand(){
        setUpEventHandler();
        String message1 = ClientDecoder.decodeMessage("WINNERS Gracz1");
        Assert.assertEquals("THERE ARE 1 WINNERS", message1);

        String message2 = ClientDecoder.decodeMessage("WINNDERS bot Daniel Gracz1 bot bot Gracz2");
        Assert.assertEquals("UNKNOWN COMMAND", message2);

        String message3 = ClientDecoder.decodeMessage("WINNERS bot Daniel Gracz1 bot bot Gracz2");
        Assert.assertEquals("THERE ARE 6 WINNERS", message3);
    }

    @Test
    public void testTurnCommands() {
        setUpEventHandler();
        String message1 = ClientDecoder.decodeMessage("YOUR_TURN");
        String message2 = ClientDecoder.decodeMessage("ENEMY_TURN");

        Assert.assertEquals("PLAYERS TURN",message1);
        Assert.assertEquals("ENEMY TURN",message2);
    }

    @Test
    public void testGameInfo() {
        setUpEventHandler();
        String message1 = ClientDecoder.decodeMessage("GAME INFO ERROR");
        String message2 = ClientDecoder.decodeMessage("GAME_INFO ERROR");
        String message3 = ClientDecoder.decodeMessage("GAME_INFO 2 0 5");
        String message4 = ClientDecoder.decodeMessage("GAME_INFO 6 0 1 2 3 4 5");

        Assert.assertEquals("UNKNOWN COMMAND",message1);
        Assert.assertEquals("BOARD GENERATING ERROR",message2);
        Assert.assertEquals("GENERATED BOARD FOR 2 PLAYERS",message3);
        Assert.assertEquals("GENERATED BOARD FOR 6 PLAYERS", message4);
    }
}
