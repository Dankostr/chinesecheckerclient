package com.app.server;

import com.app.gui.NickEntryWindow;
import org.junit.Assert;
import org.junit.Test;

public class ClientEncoderTest {

    @Test
    public void testCreateNickMessage() {
        String message1 = ClientEncoder.createNickMessage("Daniel");
        String message2= ClientEncoder.createNickMessage("Gracz");
        Assert.assertEquals("NICK Daniel", message1);
        Assert.assertEquals("NICK Gracz", message2);
    }

    @Test
    public void testSlotMessages() {
        String message1 = ClientEncoder.createJoinSlotMessage(0);
        String message2= ClientEncoder.createJoinSlotMessage(2137);
        Assert.assertEquals("JOINSLOT 0", message1);
        Assert.assertEquals("JOINSLOT 2137", message2);
        String message3 = ClientEncoder.createLeaveSlotMessage(1336);
        String message4 = ClientEncoder.createLeaveSlotMessage(31415);
        Assert.assertEquals("LEAVESLOT 1336", message3);
        Assert.assertEquals("LEAVESLOT 31415", message4);
    }

    @Test
    public void testCreateMoveMessage() {
        String message1 = ClientEncoder.createMoveMessage(0,1, 1,2);
        String message2= ClientEncoder.createMoveMessage(16, 8, 16, 7);
        Assert.assertEquals("MOVE 0 1 1 2", message1);
        Assert.assertEquals("MOVE 16 8 16 7", message2);
    }

    @Test
    public void testCreateMovesMessage() {
        String message1 = ClientEncoder.createMovesMessage(15, 4);
        String message2= ClientEncoder.createMovesMessage(0,22);
        Assert.assertEquals("MOVES 15 4", message1);
        Assert.assertEquals("MOVES 0 22", message2);
    }

    @Test
    public void testBotMessages() {
        String message1 = ClientEncoder.createAddBotMessage(1);
        String message2 = ClientEncoder.createAddBotMessage(5);
        String message3 = ClientEncoder.createRemoveBotMessage(0);
        Assert.assertEquals("ADD_BOT 1", message1);
        Assert.assertEquals("ADD_BOT 5", message2);
        Assert.assertEquals("REMOVE_BOT 0", message3);
    }

    @Test
    public void testPlayerReadyMessages() {
        String message1 = ClientEncoder.createStartGameMessage();
        Assert.assertEquals("PLAYER_READY", message1);
        String message2 = ClientEncoder.createPlayerNotReadyMessage();
        Assert.assertEquals("PLAYER_NOT_READY", message2);
        String message3 = ClientEncoder.createSkipTurnMessage();
        Assert.assertEquals("SKIP_TURN", message3);
        String message4 = ClientEncoder.createPlayerLeftGameMessage();
        Assert.assertEquals("PLAYER_HAS_LEFT", message4);
    }

}
